package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.UserCar;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserCarRepository extends JpaRepository<UserCar, Long> {

    List<UserCar> findByBuildingId(long buildingId);

    long countByCarNumberAndWechatMpUserIdAndBuildingId(String carNumber, long wechatMpUserId, long buildingId);

    long countByBuildingIdAndStatus(long buildingId, int status);

}
