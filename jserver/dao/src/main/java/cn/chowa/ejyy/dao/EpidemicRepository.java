package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.Epidemic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EpidemicRepository extends JpaRepository<Epidemic, Long> {
}
