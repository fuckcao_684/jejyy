function getComplains(type,category,community_id,refer,step,page_size,page_num){
    var sql=`
        select
            id,
            type,
            category,
            description,
            step,
            merge_id,
            created_at
        from ejyy_complain a
        where 1=1
            ${type?"and type=:type":""}
            ${category?"and category=:category":""}
            ${refer?(refer=='ower'?"and a.wechat_mp_user_id is not null":"and a.property_company_user_id is not null"):""}
            ${step?(step==4?"and (step=:step or a.merge_id is not null)":"and step=:step"):""}
        order by id desc
        limit ${(page_num - 1) * page_size},${page_size}
    `;
    return sql;
}

function getComplainsCount(type,category,community_id,refer,step){
    var sql=`
        select
            count(*)
        from ejyy_complain a
        where 1=1
            ${type?"and type=:type":""}
            ${category?"and category=:category":""}
            ${refer?(refer=='ower'?"and a.wechat_mp_user_id is not null":"and a.property_company_user_id is not null"):""}
            ${step?(step==4?"and (step=:step or a.merge_id is not null)":"and step=:step"):""}
    `;
    return sql;
}


