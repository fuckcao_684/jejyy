package cn.chowa.ejyy.model.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "ejyy_user_car")
public class UserCar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty("wechat_mp_user_id")
    private long wechatMpUserId;

    @JsonProperty("building_id")
    private long buildingId;

    @NotNull(message = "车牌号不能为空")
    @Size(max = 8, message = "车牌号长度不能大于8")
    @JsonProperty("car_number")
    private String carNumber;

    /**
     * 1 蓝牌；2黄牌
     */
    @JsonProperty("car_type")
    private int carType;

    @JsonProperty("is_new_energy")
    private int isNewEnergy;

    /**
     * 1 正常 0 删除
     */
    private int status;

    private int sync;

    @JsonProperty("created_at")
    private long createdAt;
}
