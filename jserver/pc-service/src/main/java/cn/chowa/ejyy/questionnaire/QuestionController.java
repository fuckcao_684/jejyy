package cn.chowa.ejyy.questionnaire;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.PagedData;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.common.utils.AuthUtil;
import cn.chowa.ejyy.dao.question.QuestionOptionRepository;
import cn.chowa.ejyy.dao.question.QuestionQuery;
import cn.chowa.ejyy.dao.question.QuestionRepository;
import cn.chowa.ejyy.dao.question.QuestionnaireRepository;
import cn.chowa.ejyy.model.entity.question.Question;
import cn.chowa.ejyy.model.entity.question.QuestionOption;
import cn.chowa.ejyy.model.entity.question.Questionnaire;
import cn.dev33.satoken.annotation.SaCheckRole;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/pc/questionnaire")
public class QuestionController {

    @Autowired
    private QuestionQuery questionQuery;

    @Autowired
    private QuestionnaireRepository questionnaireRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private QuestionOptionRepository questionOptionRepository;

    @SaCheckRole(Constants.RoleName.WJDC)
    @VerifyCommunity(true)
    @PostMapping("/list")
    public PagedData<ObjData> getList(@RequestBody RequestData data) {
        int published = data.getInt("published", false, "^1|0$");
        long communityId = data.getCommunityId();

        return PagedData.of(data.getPageNum(), data.getPageSize(),
                questionQuery.getQuestionnaireList(published, communityId,data.getPageSize(), data.getPageNum())
                , questionQuery.getQuestionnaireListCount(published, communityId));
    }

    /**
     * 参考文档 https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#ExampleMatcher
     * https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#specifications
     * @param data
     * @return
     */
    @SaCheckRole(Constants.RoleName.WJDC)
    @VerifyCommunity(true)
    @PostMapping("/detail")
    /**
     * {"code":200,"data":{"info":{"id":32,"title":"测试问卷1","expire":1656259200000,"published":1,"published_at":1656319950433,"created_at":1656168936057,"user_id":6,"real_name":"体验账号"},"questions":[{"title":"测试单选题","type":1,"options":[{"option_val":"答案A","id":370},{"option_val":"答案B","id":371},{"option_val":"答案C","id":372},{"option_val":"答案D","id":373}]}],"statistics":{"total":0,"options":{}}}}
     */
    public Map<?, ?> detail(@RequestBody RequestData data) {
        long id = data.getLong("id");
        long communityId = data.getCommunityId();
        Optional<Questionnaire> questionnaire = questionnaireRepository.findById(id);
        Question q = new Question();
        q.setQuestionnaireId(id);

        ExampleMatcher questionMatcher = ExampleMatcher.matching()
                .withMatcher("questionnaireId",
                        ExampleMatcher.GenericPropertyMatcher::exact)
                .withIgnorePaths("id","type")
                .withIgnoreNullValues();
        Example questExample = Example.of(q, questionMatcher);
        Optional<Question> question = questionRepository.findOne(questExample);

        QuestionOption qo = new QuestionOption();
        qo.setQuestionId(question.get().getId());
        ExampleMatcher questionOptMatcher = ExampleMatcher.matching()
                .withMatcher("questionId",ExampleMatcher.GenericPropertyMatcher::exact)
                .withIgnorePaths("id")
                .withIgnoreNullValues();
        Example questOptExample = Example.of(qo, questionOptMatcher);
        List<QuestionOption> questionOpt = questionOptionRepository.findAll(questOptExample);

        return Map.of("info",questionnaire.get(),"questions",questionOpt);
    }

    @SaCheckRole(Constants.RoleName.WJDC)
    @VerifyCommunity(true)
    @PostMapping("/create")
    @Transactional
    public Map<?, ?> create(@RequestBody RequestData data) {
        int published = data.getInt("published");
        long communityId = data.getCommunityId();
        long expire = data.getLong("expire");
        String title = data.getStr("title");
        // 保存问卷
        long qaId = questionnaireRepository.save(
                Questionnaire.builder()
                        .communityId(communityId)
                        .expire(expire)
                        .createdAt(System.currentTimeMillis())
                        .createdBy(AuthUtil.getUid())
                        .publishedAt(AuthUtil.getUid())
                        .published(published)
                        .title(title).build()
                ).getId();
        //问卷问题和选项详情
        JSONArray jsonArray = JSONUtil.parseArray(data.getJsonStr("questions"));
        List<Question> list = JSONUtil.toList(jsonArray,Question.class);
        list.forEach(e ->{
            e.setQuestionnaireId(qaId);
            long qId = questionRepository.save(e).getId();
            questionOptionRepository.save(QuestionOption.builder()
                    .questionId(qId)
                    .optionVal(e.getOptions())
                    .build());
        });

        return Map.of("id",qaId);
    }

    public static void main(String[] args) {
        System.out.println(JSONUtil.parseArray("[{\"type\":1,\"title\":\"单选题\",\"options\":[\"选项1\",\"选项2\",\"选项3\",\"选项4\"]},{\"type\":2,\"title\":\"多选题\",\"options\":[\"选项1\",\"选项2\",\"选项3\",\"选项4\"]}]"));
    }

}

