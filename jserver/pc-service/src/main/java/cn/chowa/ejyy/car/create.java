package cn.chowa.ejyy.car;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.common.utils.AuthUtil;
import cn.chowa.ejyy.dao.BuildingQuery;
import cn.chowa.ejyy.dao.ComplainRepository;
import cn.chowa.ejyy.dao.UserCarOperateLogRepository;
import cn.chowa.ejyy.dao.UserCarRepository;
import cn.chowa.ejyy.model.entity.Complain;
import cn.chowa.ejyy.model.entity.UserBuildingOperateLog;
import cn.chowa.ejyy.model.entity.UserCar;
import cn.chowa.ejyy.model.entity.UserCarOperateLog;
import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static cn.chowa.ejyy.common.Constants.building.CARPORT;
import static cn.chowa.ejyy.common.Constants.building.GARAGE;
import static cn.chowa.ejyy.common.Constants.code.*;
import static cn.chowa.ejyy.common.Constants.complain.SUBMIT_COMPLAIN_STEP;
import static cn.chowa.ejyy.common.Constants.operate_type.OPEARTE_BY_COMPANY;
import static cn.chowa.ejyy.common.Constants.status.BINDING_CAR;

@RestController("carCreate")
@RequestMapping("/pc/car")
public class create {

    @Autowired
    private UserCarRepository userCarRepository;
    @Autowired
    private BuildingQuery buildingQuery;
    @Autowired
    private UserCarOperateLogRepository userCarOperateLogRepository;

    /**
     * 录入车辆
     */
    @SaCheckRole(Constants.RoleName.CLGL)
    @VerifyCommunity(true)
    @PostMapping("/create")
    public Map<?, ?> create(@RequestBody RequestData data) {
        long wechat_mp_user_id = data.getLong("wechat_mp_user_id", true, "^\\d+$");
        long building_id = data.getLong("building_id", true, "^\\d+$");
        int is_new_energy = data.getInt("is_new_energy", true, "^0|1$");
        String car_number = data.getStr("car_number", true, 7, 8, "^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-Z0-9]{4}[A-Z0-9]{0,1}[A-Z0-9挂学警港澳]{0,1}$");
        int car_type = data.getInt("car_type", true, "^1|2$");

        long exist = userCarRepository.countByCarNumberAndWechatMpUserIdAndBuildingId(car_number, wechat_mp_user_id, building_id);
        if (exist > 0) {
            throw new CodeException(CAR_NUMBER_ALEADY_EXIST, "车辆信息已存在，请手动恢复绑定关系");
        }

        ObjData detail = buildingQuery.getBindingSetting(building_id, data.getCommunityId());

        if (detail == null || (detail.getInt("type") != CARPORT && detail.getInt("type") != GARAGE)) {
            throw new CodeException(QUERY_ILLEFAL, "非法绑定车辆");
        }

        int type = detail.getInt("type");
        long count = userCarRepository.countByBuildingIdAndStatus(building_id, BINDING_CAR);

        if (type == CARPORT && count >= detail.getInt("carport_max_car")) {
            throw new CodeException(EXCEED_ALLOW_BINDING_CAR_NUMBER, String.format("每个车位最多绑定%d辆车", detail.getInt("carport_max_car")));
        }

        if (type == GARAGE && count > detail.getInt("garage_max_car")) {
            throw new CodeException(EXCEED_ALLOW_BINDING_CAR_NUMBER, String.format("每个车库最多绑定%d辆车", detail.getInt("garage_max_car")));
        }

        UserCar userCar = userCarRepository.save(UserCar.builder()
                .wechatMpUserId(wechat_mp_user_id)
                .buildingId(building_id)
                .carNumber(car_number)
                .carType(car_type)
                .isNewEnergy(is_new_energy)
                .createdAt(System.currentTimeMillis())
                .build());

        userCarOperateLogRepository.save(
                UserCarOperateLog.builder()
                        .user_car_id(userCar.getId())
                        .property_company_user_id(AuthUtil.getUid())
                        .status(BINDING_CAR)
                        .operate_by(OPEARTE_BY_COMPANY)
                        .created_at(System.currentTimeMillis())
                        .build()
        );

        return Map.of(
                "id", userCar.getId()
        );
    }

}
