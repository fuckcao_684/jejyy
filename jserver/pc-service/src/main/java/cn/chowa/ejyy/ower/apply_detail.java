package cn.chowa.ejyy.ower;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.BuildingQuery;
import cn.chowa.ejyy.dao.OwerQuery;
import cn.dev33.satoken.annotation.SaCheckRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static cn.chowa.ejyy.common.Constants.code.QUERY_ILLEFAL;

/**
 * @ClassName apply_detail
 * @Description TODO
 * @Author ironman
 * @Date 16:11 2022/8/23
 */

@Slf4j
@RestController("owerApplyDetail")
@RequestMapping("/pc/ower")
public class apply_detail {

    @Autowired
    private OwerQuery owerQuery;

    @Autowired
    private BuildingQuery buildingQuery;

    /**
     * 查询业主档案-认证申请-申请详情
     * 备注: .whereIn('ejyy_building_info.id', info.content)
     * sql语句:  and a.id in ${content}
     * 确定是否正确?
     */
    @SaCheckRole(Constants.RoleName.YZDA)
    @VerifyCommunity(true)
    @PostMapping("/apply_detail")
    public Map<String,Object> applyDetail(@RequestBody RequestData data) {
        long id = data.getId();
        long communityId = data.getCommunityId();
        //查询用户信息
        ObjData info = owerQuery.getOwerApplyDetail(communityId,id);
        if (info == null) {
            throw new CodeException(QUERY_ILLEFAL, "非法获取用户信息");
        }
        List<ObjData> buildings = new ArrayList<ObjData>();
        if (info.getInt("replied") == 1 && info.getInt("success") == 1) {
            buildings = buildingQuery.getBuildingList(communityId,info.getLong("wechat_mp_user_id"),info.getStr("content"));
        }
        //删除 content
        info.remove("content");
        //返回结果
        return Map.of("info",info,"buildings",buildings);
    }
}
