package cn.chowa.ejyy.car;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.PagedData;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.UserCarQuery;
import cn.dev33.satoken.annotation.SaCheckRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController("carList")
@RequestMapping("/pc/car")
public class list {

    @Autowired
    private UserCarQuery userCarQuery;

    @SaCheckRole(Constants.RoleName.CLGL)
    @VerifyCommunity(true)
    @PostMapping("/list")
    public PagedData<ObjData> getList(@RequestBody RequestData data) {
        Integer is_new_energy = data.getInt("is_new_energy", false, "^0|1$");
        String car_number = data.getStr("car_number", false, 7, 8, "^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-Z0-9]{4}[A-Z0-9]{0,1}[A-Z0-9挂学警港澳]{0,1}$");
        Integer car_type = data.getInt("car_type", false, "^1|2$");
        Integer status = data.getInt("status", false, "^0|1$");
        Integer sync = data.getInt("sync", false, "^0|1$");

        return PagedData.of(data.getPageNum(), data.getPageSize(),
                userCarQuery.getUserCars(data.getCommunityId(), is_new_energy, car_number, car_type, status, sync,
                        data.getPageSize(), data.getPageNum())
                , userCarQuery.getUserCarsCount(data.getCommunityId(), is_new_energy, car_number, car_type, status, sync));
    }

}
